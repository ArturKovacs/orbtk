use crate::{prelude::*, utils::prelude::*};

property!(
    /// `Offset` describes the x- and y-axis offset of a widget.
    Offset(Point) : f64, (i32, i32)
);