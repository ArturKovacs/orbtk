use crate::{prelude::*, utils::String16};

property!(
    /// `WaterMark` describes a placeholder text.
    WaterMark(String16) : &str, String
);