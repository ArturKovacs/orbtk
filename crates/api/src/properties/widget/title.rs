use crate::prelude::*;

property!(
    /// `Title` describes the title of a widget.
    Title(String) : &str
);