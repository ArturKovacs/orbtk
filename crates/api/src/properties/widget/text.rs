use crate::{prelude::*, utils::String16};

property!(
    /// `Text` describes the text of a widget.
    Text(String16) : &str, String
);