use crate::prelude::*;

property!(
    /// `Resizeable` describes if a widget is reslizeable.
    Resizeable(bool)
);
