use crate::prelude::*;

property!(
    /// `IconFont` describes the icon font of a widget.
    IconFont(String) : &str
);