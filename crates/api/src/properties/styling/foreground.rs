use crate::{prelude::*, utils::*};

property!(
    /// `Foreground` describes the foreground brush of a visual element.
    Foreground(Brush) : &str, String
);